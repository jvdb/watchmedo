CREATE TABLE 'player' (
	'id' INTEGER PRIMARY KEY,
	'name' TEXT,
	'handle_private' TEXT,
	'handle_public' TEXT,
	'timezone' TEXT,
	'created_at' TIMESTAMP
);

CREATE TABLE 'something' (
	'id' INTEGER PRIMARY KEY,
	'player' INTEGER,
	'when' TEXT,
	'what' TEXT,
	'why' TEXT,
	'created_at' TIMESTAMP,
	FOREIGN KEY('player') REFERENCES 'player' ('id')
);

CREATE TABLE 'doing' (
	'id' INTEGER PRIMARY KEY,
	'player' INTEGER,
	'something' INTEGER,
	'outcome' TEXT,
	'created_at' TIMESTAMP,
	FOREIGN KEY('player') REFERENCES 'player' ('id'),
	FOREIGN KEY('something') REFERENCES 'something' ('id')
);
