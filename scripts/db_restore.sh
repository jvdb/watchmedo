#!/bin/sh
# gets db to read the specified file (from db/backups) and restore
# whatever is in it
set -eu
IN=""
DDIR=`dirname $0`/../
. $DDIR/env

[ $# -ne 1 ] && echo "usage: `basename $0` inputfile" && exit 1
IN=$1
[ ! -e $IN ] && echo "no such file: $IN" && exit 1

docker compose -f $DDIR/docker-compose.yml exec -T app sqlite3 $APP_DB_PATH < $IN
