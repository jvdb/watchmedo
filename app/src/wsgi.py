# watchmedo
# Copyright (c) 2022 Jozef Van den broeck, All Rights Reserved
# vim: set ts=4 sw=4 noexpandtab :
from flask import Flask, g, render_template, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired
import sqlite3
from pprint import pprint as D
from random import choice
from backports.zoneinfo import ZoneInfo
from datetime import datetime, timezone, timedelta
from string import ascii_letters, digits

app = Flask(__name__)

app.config.from_object('config')

@app.before_request
def db_setup():
	g.db_con = sqlite3.connect(app.config['DB_PATH'],
			detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
			isolation_level=None, timeout=20)
	if app.config['DEBUG']:
		g.db_con.set_trace_callback(print)
	g.db_con.execute('PRAGMA journal_mode=WAL;')
	g.db_con.execute('PRAGMA foreign_keys=ON;')
	g.db_con.row_factory = sqlite3.Row
	g.db_cur = g.db_con.cursor()
	g.db_cur.execute('BEGIN')

@app.teardown_request
def db_wrapup(error):
	db_con = getattr(g, 'db_con', None)
	db_commit = getattr(g, 'db_commit', None)

	if db_con:
		if db_commit and not error:
			db_con.commit()
		else:
			db_con.rollback()
		db_con.close()
	else:
		app.logger.error("can't find database connection in request")

def random_string(length=16):
	pool = ascii_letters + digits
	return ''.join([ choice(pool) for _ in range(length) ])
	
def model_player_add(name, timezone):

	q = """INSERT INTO
		player ('name', 'timezone', 'handle_private', 'handle_public', 'created_at')
		VALUES (?, ?, ?, ?, DATETIME('now'));"""

	handle_private = random_string(32)
	handle_public = random_string(16)

	g.db_cur.execute(q, (name, timezone, handle_private, handle_public))

	return model_player_get(handle_private)

def model_player_enrich(player):
	weekday_names = [ 'MONDAY', 'TUESDAY', 'WEDNESDAY',
			'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY' ]

	night_offset = timedelta(hours=4)

	# figure out the player's current wallclock
	# datetime.now() gives a niave timestamp, which .astimezone() will
	# take to be local to the server (it is) and transpose that to the
	# player's timezone
	player_zone = ZoneInfo(player['timezone'])
	player_now = datetime.now().astimezone(player_zone)

	# figure out what logical day it is for this player
	# since we use a night offset where everthing happening before that
	# is still part of the previous day (and everything after; the next),
	# we substract that first so we end up at yesterday's actual date
	day = player_now - night_offset
	# where does this logical day begin? well from the night offset
	day_start = day.replace(hour=0,
			minute=0, second=0, microsecond=0) + night_offset
	# and the next?
	next_day_start = day_start + timedelta(days=1)
	# how much time there is left is just looking at now vs the next
	# day's beginning
	time_left = next_day_start - player_now
	# and now to figure out what logical day it is, that's just
	# the date part of this logical day
	player_filter = [ 'DAILY' ]
	player_filter.append(weekday_names[day.weekday()])
	player_filter.append('WEEKDAY' if day.weekday() < 5 else 'WEEKENDDAY')

	# save them as UTC to keep my head sane while looking at query logs
	player['current_filter'] = player_filter
	player['now'] = player_now.astimezone(timezone.utc)
	player['day_start'] = day_start.astimezone(timezone.utc)
	player['next_day_start'] = next_day_start.astimezone(timezone.utc)
	player['day_left'] = time_left

	return player

def model_player_get(handle_private=None):

	q = """
		SELECT
			id,
			name,
			timezone,
			handle_private,
			handle_public,
			created_at
		FROM
			player
		WHERE
			handle_private=?;
		"""

	g.db_cur.execute(q, (handle_private,))

	return model_player_enrich(dict(g.db_cur.fetchone()))

def model_other_players_get(handle_private=None):

	q = """
		SELECT
			id,
			name,
			timezone,
			handle_public
		FROM
			player
		WHERE
			handle_private!=?;
		"""

	g.db_cur.execute(q, (handle_private,))

	return [ model_player_enrich(dict(row)) for row in g.db_cur.fetchall() ]

def model_something_add(player_id, when, what, why):

	q = """INSERT INTO
		something ('player', 'when', 'what', 'why', 'created_at')
		VALUES (?, ?, ?, ?, DATETIME('now'));"""

	g.db_cur.execute(q, (player_id, when, what, why))

def model_something_remove(player_id, something_id):

	q = """
		DELETE FROM
			something
		WHERE
			player=?
			AND id=?;
		"""

	g.db_cur.execute(q, (player_id, something_id))

def model_something_done(player_id, something_id):

	somethings = model_something_list(player_id)
	if something_id not in [ e['id'] for e in somethings ]:
		return

	q = """
		INSERT INTO
			doing (player, something, outcome, created_at)
		VALUES
			(?, ?, '', DATETIME('now'));
		"""

	g.db_cur.execute(q, (player_id, something_id))

def model_something_list(player_id):

	q = """
		SELECT
			id,
			"when",
			what,
			why,
			created_at
		FROM
			something
		WHERE
			player=?;
		"""
	g.db_cur.execute(q, (player_id,))

	return [ dict(row) for row in g.db_cur.fetchall() ]

def model_today(player):
	q = """
		SELECT
			s.id as "id",
			s."when" as "when",
			s.what as "what",
			s.why as "why",
			CASE
				WHEN count(d.id) > 0 THEN
					1
				ELSE
					0
			END as "done"
		FROM
			something s
			LEFT OUTER JOIN (
				SELECT
					id,
					player,
					something,
					outcome,
					created_at
				FROM
					doing
				WHERE
					created_at >= ?) d
			ON s.id = d.something
		WHERE
			s.player = ?
			AND s."when" IN (%s)
		GROUP BY
			s.id;
		""" % ', '.join([ '?' for f in player['current_filter'] ])

	args = [ player['day_start'], player['id'] ]
	args.extend(player['current_filter'])

	g.db_cur.execute(q, args)

	return [ dict(row) for row in g.db_cur.fetchall() ]

class HelloForm(FlaskForm):
	player_name = StringField(validators=[ DataRequired() ])
	player_timezone = StringField(validators=[ DataRequired() ])

@app.route('/', methods=[ 'GET', 'POST' ])
def hello():
	form = HelloForm()

	if form.validate_on_submit():
		player = model_player_add(
				form.data['player_name'],
				form.data['player_timezone'])
		g.db_commit = True
		return redirect(url_for('home',
				handle_private=player['handle_private']))

	return render_template('hello.html', form=form)

class DidSomethingForm(FlaskForm):
	something_id = StringField(validators=[ DataRequired() ])

@app.route('/<handle_private>/home', methods=[ 'GET', 'POST' ])
def home(handle_private):

	player = model_player_get(handle_private)
	others = model_other_players_get(handle_private)

	did_something_form = DidSomethingForm()
	if did_something_form.validate_on_submit():
		model_something_done(player['id'], int(did_something_form.data['something_id']))
		g.db_commit = True
		return redirect(url_for('home', handle_private=player['handle_private']))

	# again, might've changed now
	today = model_today(player)

	me = {
		'player' : {
			'name' : player['name'],
		},
		'today' : today,
		'complete' : not bool(len([ something for something in today if something['done'] == 0 ])),
		'empty' : not bool(len(today)),
	}

	others = [ {
		'player' : {
			'name' : other['name'],
		},
		'today' : model_today(other),
	} for other in others ]

	for other in others:
		other['complete'] = not bool(len([ something for something in other['today'] if something['done'] == 0 ]))
		other['empty'] = not bool(len(other['today']))

	return render_template('home.html', me=me, others=others,
			did_something_form=did_something_form, handle=handle_private)

class AddSomethingForm(FlaskForm):
	something_when = StringField(validators=[ DataRequired() ])
	something_what = StringField(validators=[ DataRequired() ])
	something_why = StringField(validators=[])

class RemoveSomethingForm(FlaskForm):
	something_id = StringField(validators=[ DataRequired() ])

@app.route('/<handle_private>/setup', methods=[ 'GET', 'POST' ])
def setup(handle_private):
	
	me = model_player_get(handle_private)

	add_something_form = AddSomethingForm()
	remove_something_form = RemoveSomethingForm()

	if add_something_form.validate_on_submit():
		model_something_add(me['id'],
				add_something_form.data['something_when'],
				add_something_form.data['something_what'],
				add_something_form.data['something_why'])
		g.db_commit = True
		return redirect(url_for('setup', handle_private=me['handle_private']))
	elif remove_something_form.validate_on_submit():
		model_something_remove(me['id'],
				remove_something_form.data['something_id'])
		g.db_commit = True
		return redirect(url_for('setup', handle_private=me['handle_private']))

	somethings = model_something_list(me['id'])

	return render_template('setup.html', me=me,
			handle=handle_private,
			somethings=somethings,
			add_something_form=add_something_form,
			remove_something_form=remove_something_form)

if __name__ == '__main__':
	app.run('0.0.0.0', port=8080, debug=True)
