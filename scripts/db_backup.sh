#!/bin/sh
# puts a db dump in a timestamped file in db/backups,
# returns the filename
OUT=${1:-`date --iso-8601=seconds`".sql"}
DDIR=`dirname $0`/../

. $DDIR/env

[ -e $OUT ] && echo "will not overwrite $OUT" && exit 1
docker compose -f $DDIR/docker-compose.yml exec -T app sqlite3 $APP_DB_PATH ".dump" > $OUT
echo `readlink -e $OUT`
